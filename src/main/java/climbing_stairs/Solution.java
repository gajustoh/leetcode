package climbing_stairs;

public class Solution {
    public static void main(String[] args) {
        System.out.println(climbStairs(4));
    }
    public static int climbStairs(int n) {
        if (n == 2)
            return 2;
        if (n == 1)
            return 1;
        int b1 = 1, b2 = 2;
        int ways = 0;
        for (int i = 3; i <= n; i++) {
            ways = b1 + b2;
            b1 = b2;
            b2 = ways;
        }
        return ways;
    }
}
