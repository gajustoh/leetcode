package merge_sorted_array;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        int[] nums1 = new int[]{1,2,3,0,0, 0}, nums2 = new int[]{2,5, 6};
        merge(nums1, 3, nums2, 3);

        for (int num : nums1 )
            System.out.print(num + " ");
    }

    /**
     * First option with an additional list
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
//    public static void merge(int[] nums1, int m, int[] nums2, int n) {
//        int[] result = new int[m + n];
//        int currentIndex = 0, i = 0, j = 0;
//        while (currentIndex <(m + n)) {
//            if (i>=m) {
//                result[currentIndex] = nums2[j++];
//            }
//            else if (j>=n) {
//                result[currentIndex] = nums1[i++];
//            }
//            else if (nums1[i] <= nums2[j]) {
//                result[currentIndex] = nums1[i];
//                i++;
//            }
//            else {
//                result[currentIndex] = nums2[j];
//                j++;
//            }
//            currentIndex++;
//        }
//        System.arraycopy(result, 0, nums1, 0, result.length);
//    }

    /**
     * Option 2 without additional list
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1, j = n - 1,  currentIndex = m + n - 1;
        while (currentIndex >= 0) {
            nums1[currentIndex--] = (i < 0) ? nums2[j--] :
                    (j < 0) ? nums1[i--] :
                            (nums1[i] >= nums2[j]) ? nums1[i--]
                                    : nums2[j--];
        }
    }
}