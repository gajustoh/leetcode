package remove_duplicates_from_sorted_list;

import java.util.HashMap;
import java.util.Map;

class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }
public class Solution {
    public static void main(String[] args) {
        var result = deleteDuplicates(mapToListNode(new int[]{1, 1, 1, 1, 2, 3, 3, 3, 3, 4}));
        while(result != null) {
            System.out.println(result.val);
            result= result.next;
        }
    }
    private static ListNode mapToListNode(int[] arr) {
        ListNode head = new ListNode(arr[0]);
        ListNode current = head;
        for (int i = 1; i < arr.length; i++) {
            current.next = new ListNode(arr[i]);
            current = current.next;
        }
        return head;
    }
    public static ListNode deleteDuplicates(ListNode head) {
        ListNode current = head;
        while(current.next != null ) {
            if (current.val == current.next.val) {
                current.next = current.next.next;
            }
            else{
                current = current.next;
            }
        }
        return head;
    }
}