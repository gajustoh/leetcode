package external;

import java.io.IOException;
import java.util.List;

public class TestException {
    public static void main(String[] args) {
        List<Dog> dogs = List.of(new Dog("pedro"), new Dog("juan"), new Dog("sandro"), new Dog("pedro"));

        try {
            int s = 1/0;
        }
//        catch (RuntimeException e){
//            System.out.println("ae = " + e);
//        } catch (Exception e) {
//            System.out.println("re = " + e);
//        }
        catch (ArrayStoreException e) {
            System.out.println("e = " + e);
        }
        finally {
            System.out.println(" = ");
        }

        long count = dogs.stream().filter(x -> x.getName().equals("pedjro")).count();
        System.out.println("count = " + count);


    }

//    public static void main(String[] args) {
//        try {
//            // Llamada a un método que puede lanzar IOException
//            leerArchivo("archivo.txt");
//        } catch (IOException e) {
//            // Manejo de la excepción
//            System.out.println("Error al leer el archivo: " + e.getMessage());
//        }
//    }

    public static void leerArchivo(String nombreArchivo) throws IOException {
        // Código que puede lanzar una IOException
        // Por ejemplo, intentar abrir y leer un archivo
        throw new IOException("No se puede leer el archivo: " + nombreArchivo);
    }
}
