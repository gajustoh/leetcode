package Search_Insert_Position;

class Solution {
    public static void main(String[] args) {
        System.out.println("-> " + searchInsert(new int[]{1,3, 5, 6}, 7));
    }
    public static int searchInsert(int[] nums, int target) {
        int min = 0, max = nums.length -1, centro = (min + max)  / 2;
        while (min <= max) {
            centro =(max + min) / 2;
            if (target == nums[centro])
                return centro;
            if (target < nums[centro]) {
                max = centro - 1;
            }
            else
                min = centro + 1;
        }
        return min;
    }
}