package animal;
public class Animal {
    protected String nombre;

    // Constructor
    public Animal(String nombre) {
        this.nombre = nombre;
    }

    // Método protegido
    protected void saludar() {
        System.out.println("Hola, soy un animal llamado " + nombre);
    }
}