package animal;

public class Veterinario {

    public static void main(String[] args) {
        examinar(new Animal("perro"));
    }
    // Método que utiliza el método protegido de la clase Animal
    public static void examinar(Animal animal) {
        animal.saludar(); // El método saludar() es accesible aquí ya que es protected
        System.out.println("El animal " + animal.nombre + " está siendo examinado");
    }
}
